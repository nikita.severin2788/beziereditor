﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BezierEditor : MonoBehaviour
{
    [Range(0.001f, 0.5f)]
    public float LineStep = 0.1f;
    private List<Vector3> Points = new List<Vector3>();

    public void OnDrawGizmos() {

        if(GetComponent<BezierPointsPool>()){
            Points = GetComponent<BezierPointsPool>().GetPointList();

            if(Points.Count > 0){

                float LineLengtch = 0f;

                Vector3 previousPoint = Bezier.GetPosition(Points,0f);

                while(LineLengtch < Bezier.MaxT(Points) - LineStep){

                    LineLengtch += LineStep;

                    Vector3 setPoint = Bezier.GetPosition(Points,LineLengtch);

                    Gizmos.DrawLine(previousPoint,setPoint);

                    previousPoint = setPoint;

                }

                previousPoint = Bezier.GetPosition(Points,0f);
            }
        }
    }
}
