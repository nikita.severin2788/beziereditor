﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Bezier
{
    public static Vector3 GetPosition(List<Vector3> pointList, float t) {

        if(t > Bezier.MaxT(pointList) - 0.01f) t = 0; //Проверка на trashold;

        Vector3 setPostion;

        int i = 0;

        int floorNumber = Mathf.FloorToInt(t);

        if(floorNumber > 0 && floorNumber * 3 < pointList.Count -1) i = floorNumber * 3;

        float setT = t - floorNumber;


        Vector3 p0 = pointList[i];
        Vector3 p1 = pointList[i+1];
        Vector3 p2 = pointList[i+2];
        Vector3 p3 = pointList[i+3];

        setPostion = Mathf.Pow((1f - setT),3) * p0 + 
            3 * Mathf.Pow((1f - setT),2)* setT * p1 + 
            3 * (1f - setT) * Mathf.Pow(setT,2) * p2 + 
            Mathf.Pow(setT,3) * p3;

        return setPostion; 
    }

    public static Vector3 GetRotation(List<Vector3>pointList, float t) {

        if(t > Bezier.MaxT(pointList) - 0.01f) t = 0; //Проверка на trashold;

        Vector3 setRotation;

        int i = 0;

        int floorNumber = Mathf.FloorToInt(t);

        if(floorNumber > 0 && floorNumber * 3 < pointList.Count - 1) i = floorNumber * 3;

        float setT = t - floorNumber;


        Vector3 p0 = pointList[i];
        Vector3 p1 = pointList[i + 1];
        Vector3 p2 = pointList[i + 2];
        Vector3 p3 = pointList[i + 3];

        setRotation = 3 * Mathf.Pow(1 - setT,2) * (p1 - p0) +
            6 * (1 - setT) * setT * (p2 - p1) +
            3 * Mathf.Pow(setT,2) * (p3 - p2);

        return setRotation;
    }

    public static float MaxT(List<Vector3> pointList){

        float max = 0;

        max = Mathf.FloorToInt((pointList.Count - 1) / 3);

        return max;
    }
}
