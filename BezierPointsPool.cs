﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class BezierPointsPool : MonoBehaviour
{

    public List<Transform> TransformPoints = new List<Transform>();
    private List<Vector3> Points = new List<Vector3>();

    public bool Recalculate = true;

    private void Update() {

        if(Recalculate){
            Points.Clear();

            foreach(Transform setTransform in TransformPoints) {
                Points.Add(setTransform.position);
            }
        }
    }
    
    
    public List<Vector3> GetPointList() {
        return Points;
    }
}
